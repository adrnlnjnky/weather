package retriever

import (
	"testing"
	"time"

	dbaccess "Gitlab.com/adrnlnjnky/Weather/Dbaccess"
)

var (
	lowf  = 8.00
	highf = 135.00
	lowC  = -10.00
	highC = 80.00
)

func TestCurrentTempOW(t *testing.T) {
	db := dbaccess.GormOpenDB("devdatabase")
	weather := CurrentTempOW(db)

	t.Run("Test that Time is Current", func(t *testing.T) {
		got := weather.CreatedAt
		want := time.Now()

		if got == want {
			t.Errorf("Got ain't Want: got: %v || want %v", got, want)
		}
	})

	t.Run("Test Temp is within earth tolerances and not 0", func(t *testing.T) {
		got := weather.Temp
		if got == 0 {
			t.Errorf("There is a 0 reading")
		}
		if got > highf {
			t.Errorf("%+v: Seems to hot to be accurate.", got)
		}
		if got < lowf {
			t.Errorf("%+v: Seems to cold to be accurate.", got)
		}
	})

	t.Run("Test that Feels Like is within earth tolerances", func(t *testing.T) {
		got := weather.FeelsLike
		if got == 0 {
			t.Errorf("There is a 0 reading")
		}
		if got > highf {
			t.Errorf("%+v: Seems to hot to be accurate.", got)
		}
		if got < lowf {
			t.Errorf("%+v: Seems to cold to be accurate.", got)
		}
	})

	t.Run("Test that TempMin is within earth tolerances", func(t *testing.T) {
		got := weather.TempMin
		if got == 0 {
			t.Errorf("There is a 0 reading")
		}
		if got > highf {
			t.Errorf("%+v: Seems to hot to be accurate.", got)
		}
		if got < lowf {
			t.Errorf("%+v: Seems to cold to be accurate.", got)
		}
	})

	t.Run("Test that TempMax is within earth tolerances", func(t *testing.T) {
		got := weather.TempMax
		if got == 0 {
			t.Errorf("There is a 0 reading")
		}
		if got > highf {
			t.Errorf("%+v: Seems to hot to be accurate.", got)
		}
		if got < lowf {
			t.Errorf("%+v: Seems to cold to be accurate.", got)
		}
	})

	t.Run("Test that Pressure is within earth tolerances", func(t *testing.T) {
		got := weather.Pressure
		if got > 2000 {
			t.Errorf("%+v: Seems to hot to be accurate.", got)
		}
		if got < 500 {
			t.Errorf("%+v: Seems to cold to be accurate.", got)
		}
	})

	t.Run("Test that Humidity is within earth tolerances", func(t *testing.T) {
		got := weather.Humidity
		if got > 100 {
			t.Errorf("%+v: Seems to hot to be accurate.", got)
		}
		if got < 2 {
			t.Errorf("%+v: Seems to cold to be accurate.", got)
		}
	})

	t.Run("Test that Humidity is within earth tolerances", func(t *testing.T) {
		got := weather.Sunrise
		if got == 0 {
			t.Errorf("%+v: Seems odd time is zero", got)
		}
	})

}

package retriever

import (
	models "Gitlab.com/adrnlnjnky/Weather/Models"
	"gorm.io/gorm"
)

// Forecast2NWS returns the last recorded weather readings from the Open
// Weather API
func Forecast2NWS(db *gorm.DB, place string) models.NWSForecast {
	db.AutoMigrate(&models.NWSForecast{})
	var weather models.NWSForecast
	mark := "Now()  >= interval ' 24 hours'"
	err := db.Model(&models.NWSForecast{}).Where("time < ?", mark).Find(&weather)
	if err.Error != nil {
		panic("handle this error please")
	}
	if err.RowsAffected < 1 {
		panic("handle this situation please")
	}
	return weather
}

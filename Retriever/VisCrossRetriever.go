package retriever

import (
	models "Gitlab.com/adrnlnjnky/Weather/Models"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Forecast2VCW returns the last recorded weather readings from the Open
// Weather API
func Forecast2VCW(db *gorm.DB, place string) models.VisualCrossingForecast {
	db.AutoMigrate(&models.VisualCrossingForecast{})
	var weather models.VisualCrossingForecast
	// mark := "NVCW()  >= interval ' 24 hours'"
	// err := db.Model(&models.OnyxForecastVisual{}).Where("time < ?", mark).Find(&weather)
	err := db.Model(&models.VisualCrossingForecast{}).Last(&weather)
	if err.Error != nil {
		panic("handle this error please")
	}
	if err.RowsAffected < 1 {
		panic("handle this situation please")
	}
	return weather
}

// GetVisCrossCurrent Working on using the in memory values for most recent data, I mean it's in the buffer so might as well expose it and
// use it.  Right?
func GetVisCrossCurrent(ctx *gin.Context) models.VisualCrossingCurrent {
	weather := models.VisualCrossingCurrent{}
	format := ctx.GetHeader("application/type")
	switch {
	case format == "":
		return weather
	case format == "application/json":
		ctx.BindJSON(&weather)
	case format == "application/xml":
		ctx.BindXML(&weather)
	case format == "application/querry":
		ctx.BindQuery(weather)
	}
	return weather
}

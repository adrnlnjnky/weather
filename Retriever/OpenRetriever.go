package retriever

import (
	"log"

	"gorm.io/gorm"

	dbstuff "Gitlab.com/adrnlnjnky/Weather/Dbaccess"
	models "Gitlab.com/adrnlnjnky/Weather/Models"
)

// CurrentTempOW returns the last recorded weather readings from the Open
// Weather API
func CurrentTempOW(db *gorm.DB) models.OWCurrent {
	db.AutoMigrate(&models.OWCurrent{})
	var weather models.OWCurrent
	var store map[string]interface{}
	err := db.Model(&models.OWCurrent{}).Last(&weather)
	err2 := db.Model(&models.OWCurrent{}).Last(&store)
	if err.Error != nil {
		log.Printf("Received an Error while getting the data: %v", err.Error)
	}
	if err2.Error != nil {
		log.Printf("Received an Error while getting the data: %v", err.Error)
	}
	if err.RowsAffected < 1 {
		log.Printf("Did not affect any rows while getting the data: %v", err.RowsAffected)
	}
	weather.Temp = faren(weather.Temp)
	return weather
}

// Forecast2OW returns the last recorded weather readings from the Open
// Weather API
func Forecast2OW(db *gorm.DB, place string) models.OWHourly {
	db.AutoMigrate(&models.OWHourly{})
	var weather models.OWHourly
	mark := "NOW()  >= interval ' 24 hours'"
	err := db.Model(&models.OWHourly{}).Where("time < ?", mark).Find(&weather)
	if err.Error != nil {
		panic("handle this error please")
	}
	if err.RowsAffected < 1 {
		panic("handle this situation please")
	}
	return weather
}

func Temp() float64 {
	db := dbstuff.GormOpenDB("devdatabase")
	raw := CurrentTempOW(db)
	temp := faren(raw.Temp)

	return temp
}

func faren(kelvin float64) float64 {
	return (9.0/5.0*(kelvin-273.0) + 32.0)
}

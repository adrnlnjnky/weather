package retriever

import (
	"testing"
	"time"

	dbaccess "Gitlab.com/adrnlnjnky/Weather/Dbaccess"
)

func TestForecast2VCW(t *testing.T) {
	db := dbaccess.GormOpenDB("devdatabase")
	weather := Forecast2VCW(db, "NoPlace")

	t.Run("Test that Conditions has data", func(t *testing.T) {
		got := weather.Conditions
		want := "something"

		if got == "" {
			t.Errorf("got want mismatch: got %+v || want %+v", got, want)
		}
	})

	t.Run("Test that Time is Current", func(t *testing.T) {
		got := weather.CreatedAt
		want := time.Now()

		if got == want {
			t.Errorf("Got ain't Want: got: %v || want %v", got, want)
		}
	})

	t.Run("Test that Temp is within earth tolerances", func(t *testing.T) {
		got := weather.Temp
		if got > 150 {
			t.Errorf("%+v: Seems to hot to be accurate.", got)
		}
		if got < -50 {
			t.Errorf("%+v: Seems to cold to be accurate.", got)
		}
	})
}

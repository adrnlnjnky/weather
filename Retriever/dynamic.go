package retriever

import (
	"encoding/json"
	"fmt"
	"net/http"

	dbaccess "Gitlab.com/adrnlnjnky/Weather/Dbaccess"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// DynamicLearning is a good place for me to work out the websocket functioning
func DynamicLearning(ctx *gin.Context) {
	w := ctx.Writer
	r := ctx.Request
	Dynamic(w, r)

	ctx.HTML(http.StatusOK, "index.tmpl", "something")
}

// cause I gotta pass ctx then get w and r.  I imagine I'll clean this up one day. My wife is haveing a hilariaous vidoe
// conference with the three test friends and I'm barely hanging on.
func Dynamic(w http.ResponseWriter, r *http.Request) {
	db := dbaccess.GormOpenDB("devdatabase")

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println(err)
	}

	for {
		weather := CurrentTempOW(db)
		temp := weather.Temp
		var info []byte
		info, err := json.Marshal(temp)
		if err != nil {
			fmt.Println(err)
			return
		}

		err = conn.WriteMessage(websocket.TextMessage, info)
		if err != nil {
			fmt.Println(err)
			break
		}
	}
}

package retriever

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	dbaccess "Gitlab.com/adrnlnjnky/Weather/Dbaccess"
	models "Gitlab.com/adrnlnjnky/Weather/Models"
)

// WeatherReporter is an interface I made for some reason
type WeatherReporter interface {
	DBset()
	GetAll()
	current()
}

// DBset sets opens an instance of the database and links it to all of the
// related structs.
func DBset(ctx *gin.Context) *gorm.DB {
	// database := ctx.Param("database")
	db := dbaccess.GormOpenDB("devdatabase")
	// db := dbaccess.GormOpenDB(database)

	return db
}

// ViewCurrent is
// func ViewCurrent(ctx *gin.Context) {
func ViewCurrent(ctx *gin.Context) {
	db := DBset(ctx)
	time := time.Now()
	owCurrent := current(db)
	owWeather := oWforecast(db)
	visWeather := cRforecast(db)
	natWeather := nWforecast(db)

	titles := gin.H{
		"feel":     "Feels Like",
		"humidity": "humidity",
		"icon":     "icon",
		"max":      "Predicted High",
		"min":      "Predicted Low",
		"pres":     "pressure",
		"precip":   "Precipitation",
		"sunrise":  "sunrise",
		"sunset":   "sunset",
		"temp":     "temp",
		"time":     "time",
		"vis":      "visibility",
		"when":     "Forecasted Time",
		"wind":     "Wind speed",
		"year":     "year",
	}

	current := gin.H{
		"time":     owCurrent.CreatedAt.Unix,
		"temp":     owCurrent.Temp,
		"feel":     owCurrent.FeelsLike,
		"wind":     owCurrent.WindSpeed,
		"humidity": owCurrent.Humidity,
		"pres":     owCurrent.Pressure,
		"sunrise":  owCurrent.Sunrise,
		"sunset":   owCurrent.Sunset,
		// "icon":     owCurrent.OWCWeather.Icon,
	}

	open := gin.H{
		"time":     owWeather.CreatedAt.Unix,
		"temp":     owWeather.Temp,
		"feel":     owWeather.FeelsLike,
		"wind":     owWeather.WindSpeed,
		"humidity": owWeather.Humidity,
		"pres":     owWeather.Pressure,
	}

	visual := gin.H{
		"time":     visWeather.Datetime,
		"temp":     visWeather.Temp,
		"maxtemp":  visWeather.Maxt,
		"mintemp":  visWeather.Mint,
		"precip":   visWeather.Precip,
		"wind":     visWeather.Wspd,
		"humidity": visWeather.Humidity,
		"pres":     visWeather.Sealevelpressure,
		"vis":      visWeather.Visibility,
	}

	national := gin.H{
		"time": natWeather.UpdatedAt,
		"temp": natWeather.NWSHour,
		// "tempU": natWeather.Temperatureunit,
		// "wind":  natWeather.Windspeed,
		// "IsDay": natWeather.Isdaytime,
		// "Short": natWeather.Shortforecast,
	}

	weather := gin.H{
		"time":     time,
		"titles":   titles,
		"current":  current,
		"open":     open,
		"visual":   visual,
		"national": national,
	}

	ctx.HTML(http.StatusOK, "index.tmpl", weather)
}

// GetAll is gonna send everything for a website
func GetAll(ctx *gin.Context) {
	db := DBset(ctx)

	current := current(db)
	OWF := oWforecast(db)
	NWF := nWforecast(db)
	CWF := cRforecast(db)

	ctx.JSON(200, current)
	ctx.JSON(200, OWF)
	ctx.JSON(200, NWF)
	ctx.JSON(200, CWF)

}

func current(db *gorm.DB) models.OWCurrent {
	table := models.OWCurrent{}
	var weather models.OWCurrent
	db.Model(&table).Last(&weather)
	return weather
}

func oWforecast(db *gorm.DB) models.OWHourly {
	table := models.OWHourly{}
	var weather models.OWHourly
	db.Model(&table).Last(&weather)
	return weather
}

func nWforecast(db *gorm.DB) models.NWSForecast {
	table := models.NWSForecast{}
	var weather models.NWSForecast
	db.Model(&table).Last(&weather)
	return weather
}

func cRforecast(db *gorm.DB) models.VisualCrossingForecast {
	table := models.VisualCrossingForecast{}
	var weather models.VisualCrossingForecast
	db.Model(&table).Last(&weather)
	return weather
}

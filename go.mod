module Gitlab.com/adrnlnjnky/Weather

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3
	github.com/go-delve/delve v1.5.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.8.0
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/mgutz/logxi v0.0.0-20161027140823-aebf8a7d67ab
	github.com/mitchellh/mapstructure v1.4.0
	github.com/rs/cors v1.7.0
	github.com/tpkeeper/gin-dump v1.0.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	google.golang.org/appengine v1.6.7
	gopkg.in/robfig/cron.v2 v2.0.0-20150107220207-be2e0b0deed5
	gorm.io/driver/postgres v1.0.2
	gorm.io/gorm v1.20.2
)

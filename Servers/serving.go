package serve

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	helpers "Gitlab.com/adrnlnjnky/Weather/Helpers"
	middlewares "Gitlab.com/adrnlnjnky/Weather/Middlewares"
	retriever "Gitlab.com/adrnlnjnky/Weather/Retriever"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
	gindump "github.com/tpkeeper/gin-dump"
	"gorm.io/gorm"
)

// setupLogOutput will send the logger data to a file instead of the monitor
func setupLogOutput() {
	f, err := os.Create("Log/gin.log")
	if err != nil {
		log.Panic("something went wrong with the Log output setup. Error: ", err)
	}
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
}

var wsupgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func wshandler(w http.ResponseWriter, r *http.Request) {
	conn, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Printf("Failed to set websocket upgrade: %+v", err)
		return
	}
	for {
		t, msg, err := conn.ReadMessage()
		if err != nil {
			break
		}
		conn.WriteMessage(t, msg)
	}
}

// PushStuff is going to push some data to some boxes.
func PushStuff(w http.ResponseWriter, r *http.Request) {
	conn, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Printf("Failed to set websocket upgrade: %+v", err)
		return
	}
	for {
		i := 0
		temp := fmt.Sprintf("Temp: %.0f :%v", retriever.Temp(), i)
		JSONblob, _ := json.Marshal(temp)
		err = conn.WriteMessage(websocket.TextMessage, JSONblob)
		if err != nil {
			fmt.Println(err)
			break
		}
		i++
		time.Sleep(1 * time.Second)
	}
	conn.Close()
	fmt.Println("Client unsubscribed")
}

// ServeWeather is my setup server I plan to abstract out from here to get all the websocket activity I want.
func SomeWeather(db *gorm.DB) {

	helpers.Setenv()
	port := os.Getenv("PORT")
	route := os.Getenv("ROUTE")

	setupLogOutput()

	server := gin.New()
	server.Use(
		gin.Recovery(),
		gin.Logger(),
		gindump.Dump(),
	)
	server.Static("/CSS", "./Templates/CSS")
	server.LoadHTMLGlob("Templates/*.tmpl")

	apiRoute := server.Group("/api", middlewares.BasicAuth())
	{
		apiRoute.GET("/weather", retriever.GetAll)
	}

	viewRoute := server.Group("view")
	{
		viewRoute.GET("/weather", retriever.ViewCurrent)
		viewRoute.GET("/test", func(c *gin.Context) {
			c.HTML(200, "weather.tmpl", nil)
		})
		viewRoute.GET("/ws", func(c *gin.Context) {
			PushStuff(c.Writer, c.Request)
		})
	}

	if route == "" {
		route = "localhost:"
	}
	if port == "" {
		port = "7070"
	}
	server.Run(route + port)
}

package helpers

import (
	"log"

	"github.com/joho/godotenv"
)

func Setenv() {
	e := godotenv.Load()
	if e != nil {
		log.Fatal("Error loading environment")
	}
}

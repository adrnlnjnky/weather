package helpers

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

// DbError responds to errors when interacting with the database
func DbError(ctx *gin.Context, err error) {
	errNote := fmt.Sprintf("Something went wrong with the database, got this error: %+v\n", err)
	ctx.JSON(404, errNote)
}

// NoRows responds when rows in the database should have been affected but no
// rows were.
func NoRows(ctx *gin.Context) {
	note := fmt.Sprint("Nobody found in your contact list?")
	ctx.JSON(204, note)
}

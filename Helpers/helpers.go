package helpers

import (
	"encoding/json"
	"fmt"
)

func handler() {}

// ExtractJSON extracts the JSON into a map and andles any errors
func ExtractJSON(jsonBlob []byte) (map[string]interface{}, error) {
	var data map[string]interface{}
	err := json.Unmarshal(jsonBlob, &data)
	if err != nil {
		fmt.Println("error:", err)
		return nil, err
	}
	return data, nil
}

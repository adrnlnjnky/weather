package main

import (
	dbaccess "Gitlab.com/adrnlnjnky/Weather/Dbaccess"
	recorder "Gitlab.com/adrnlnjnky/Weather/Recorder"
	serve "Gitlab.com/adrnlnjnky/Weather/Servers"
)

var database = "devdatabase"

func main() {

	db := dbaccess.GormOpenDB(database)
	go recorder.RecordKeeper(db)

	serve.SomeWeather(db)
}

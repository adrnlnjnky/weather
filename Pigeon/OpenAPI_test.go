package pigeon

import (
	"log"
	"testing"

	"github.com/joho/godotenv"
)

// TestCallZip test call to Open Weather Service.
func TestCallZip(t *testing.T) {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading environment")
	}
	got := CDCallZIP("93255")
	if got == nil {
		log.Println("Nothing was returned")
	}
}

package pigeon

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func somefunc() {
	// weatherbit API call sample -> Rapidapi
	url := "https://weatherbit-v1-mashape.p.rapidapi.com/current"
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("x-rapidapi-key", "SIGN-UP-FOR-KEY")
	req.Header.Add("x-rapidapi-host", "weatherbit-v1-mashape.p.rapidapi.com")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	fmt.Println(res)
	fmt.Println(string(body))
}

package pigeon

import (
	"io/ioutil"
	"log"
	"net/http"
)

// VisualCrossingWeatherAPI makes the request from Visual Crossing Weathers API
// func VisualCrossingWeatherAPI() map[string]interface{} {
func VisualCrossingWeatherAPI() []byte {

	url := "https://visual-crossing-weather.p.rapidapi.com/forecast?location=35.8%2C%20-118.1&aggregateHours=24&contentType=json&shortColumnNames=true&unitGroup=us"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("x-rapidapi-key", "761328022bmshf8d5f60d88d4944p19a1fajsnaad5af5c5639")
	req.Header.Add("x-rapidapi-host", "visual-crossing-weather.p.rapidapi.com")
	res, _ := http.DefaultClient.Do(req)

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatal(err)
	}
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	return body
}

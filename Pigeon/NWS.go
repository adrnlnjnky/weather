package pigeon

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

var dumpRaw = false

// NWSAPIs makes the request from the National Weather Service API
func NWSAPIs(lat float64, lon float64) []byte {
	urlString := fmt.Sprintf("https://api.weather.gov/points/%v,%v", lat, lon)
	u, err := url.Parse(urlString)
	if err != nil {
		errorParsing("NWSAPI", err)
	}
	res, err := http.Get(u.String())
	if err != nil {
		errorGetting("NWSAPI", err)
	}
	jsonBlob, err := ioutil.ReadAll(res.Body)
	if err != nil {
		errorReading("NWSAPI", err)
	}
	res.Body.Close()
	return jsonBlob
}

// NWSForecast makes the request from the National Weather Service API
// func NWSForecast(api string) map[string]interface{} {
func NWSForecast(api string) []byte {
	urlString := fmt.Sprintf(api)
	u, err := url.Parse(urlString)
	if err != nil {
		errorParsing("NWSForecast", err)
	}
	res, err := http.Get(u.String())
	if err != nil {
		errorGetting("NWSForecast", err)
	}
	jsonBlob, err := ioutil.ReadAll(res.Body)
	if err != nil {
		errorReading("NWSForecast", err)
	}
	res.Body.Close()
	return jsonBlob
}

func errorParsing(where string, err error) {
	log.Printf("an Error in the Parsing at %v  :: %v", where, err)
}
func errorGetting(where string, err error) {
	log.Printf("an Error in the getting at %v  :: %v", where, err)
}
func errorReading(where string, err error) {
	log.Printf("an Error in the Reading at %v  :: %v", where, err)
}

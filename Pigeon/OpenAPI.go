package pigeon

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"

	"github.com/joho/godotenv"
)

// OpenAPIForecast makes a call
// func OpenAPIForecast(lat float64, lon float64) map[string]interface{} {
func OpenAPIForecast(lat float64, lon float64) []byte { // err := godotenv.Load(".env")
	// if err != nil {
	// log.Fatal("Error loading environment")
	// }
	// api := os.Getenv("OWM_API_KEY")
	api := "90d8b9f81f02cffe116dcbe1b39551d8"
	urlString := fmt.Sprintf("http://api.openweathermap.org/data/2.5/onecall?lat=%f&lon=%f&APPID=%s", lat, lon, api)
	u, err := url.Parse(urlString)
	res, err := http.Get(u.String())
	if err != nil {
		log.Fatal(err)
	}
	jsonBlob, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	return jsonBlob

}

// CDCallZIP makes a call too
func CDCallZIP(zip string) map[string]interface{} {

	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading environment")
	}
	// api := os.Getenv("OWM_API_KEY")
	api := "90d8b9f81f02cffe116dcbe1b39551d8"

	urlString := fmt.Sprintf("http://api.openweathermap.org/data/2.5/weather?zip=%s&APPID=%s", zip, api)
	u, err := url.Parse(urlString)
	res, err := http.Get(u.String())
	if err != nil {
		log.Fatal(err)
	}
	jsonBlob, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	var data map[string]interface{}
	if dumpRaw {
		fmt.Printf("blob = %s\n\n", jsonBlob)
	}
	err = json.Unmarshal(jsonBlob, &data)
	if err != nil {
		fmt.Println("error:", err)
	}
	return data
}

package main

import (
	dbaccess "Gitlab.com/adrnlnjnky/Weather/Librarian"
	recorder "Gitlab.com/adrnlnjnky/Weather/Recorder"
)

func main() {

	db := dbaccess.GormOpenDB()
	recorder.RecordKeeper(db)
}

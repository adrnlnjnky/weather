package models

import (
	"gorm.io/gorm"
)

// OpenWeather is the top of the tree, trying to get json to unmarshal the whole thing.
type OpenWeather struct {
	gorm.Model
	// Alerts         []string
	Current        OWCurrent  `json:"current" gorm:"foreignKey:OpenWeatherID"`
	Minutely       []OWMinute `json:"minutely" gorm:"foreignKey:OpenWeatherID"`
	Hourly         []OWHourly `json:"hourly" gorm:"foreignKey:OpenWeatherID"`
	Daily          []OWDaily  `json:"daily" gorm:"foreignKey:ID"`
	Lat            float64    `json:"lat"`
	Lon            float64    `json:"lon"`
	Timezone       string     `json:"timezone"`
	TimezoneOffset float64    `json:"timezone_offset"`
}

// type OWMinutes []OWMinute
// type OWHourlies []OWMinute
// type OWDailies []OWDaily

// OWCurrent is just that
type OWCurrent struct {
	gorm.Model
	Clouds        float64
	DewPoint      float64 `json:"dew_point"`
	Dt            float64
	FeelsLike     float64 `json:"feels_like"`
	Humidity      float64
	OpenWeatherID uint
	Pressure      float64
	Sunrise       float64
	Sunset        float64
	Temp          float64
	Uvi           float64
	Visibility    float64
	Weather       OWCWeather `json:"weather" gorm:"foreignKey:OWCurrentID"`
	WindDeg       float64    `json:"wind_deg"`
	WindGust      float64    `json:"wind_gust"`
	WindSpeed     float64    `json:"wind_speed"`
}

// OWCWeather helper struct in the maze
type OWCWeather struct {
	OWCurrentID uint
	Description string
	Icon        string
	Id          int    `json:"Id"`
	Main        string `json:"main"`
}

// OWMinute records precipitation infor
type OWMinute struct {
	gorm.Model
	Dt            float64
	Precipitation float64
	OpenWeatherID uint
}

// OWHourly is just this
type OWHourly struct {
	gorm.Model
	Dt         float64 `gorm:"dt"`
	Alerts     string  `gorm:"type:text"`
	Clouds     float64 `gorm:"type:real"`
	DewPoint   float64 `gorm:"type:real"`
	FeelsLike  float64 `gorm:"type:real"`
	Humidity   float64 `gorm:"type:real"`
	Pop        float64 `gorm:"type:real"`
	Pressure   float64 `gorm:"type:real"`
	Temp       float64 `gorm:"type:real"`
	Visibility float64 `gorm:"type:real"`
	Uvi        float64 `gorm:"type:real"`
	WindDeg    float64 `gorm:"type:real"`
	WindSpeed  float64 `gorm:"type:real"`
	// weather       map[string]interface{}
	Weather       []OWHWeather
	OpenWeatherID uint
}

// OWHWeather is a struct
type OWHWeather struct {
	gorm.Model
	main        string
	description string
	icon        string
	OWHourlyID  uint
}

// OWDaily is the daily type
type OWDaily struct {
	gorm.Model
	Alerts        string `gorm:"default:No Alerts"`
	Clouds        float64
	DewPoint      float64
	Dt            float64
	FeelsLike     []OWDFeelsLike
	Humidity      float64
	Pop           float64
	Pressure      float64
	Rain          float64
	Sunrise       float64
	Sunset        float64
	Snow          float64
	Temp          OWDTemp
	Uvi           float64
	Weather       []OWDWeather `json:"weather"`
	WindDeg       float64
	WindSpeed     float64 `gorm:"default:0"`
	OpenWeatherID uint
}

// OWDWeather is yet more of the maze
type OWDWeather struct {
	gorm.Model
	Description string
	Icon        string
	Main        string
	OWDailyID   uint
}

// OWDTemp is yet more of the maze
type OWDTemp struct {
	gorm.Model
	Night     float64
	Eve       float64
	Morn      float64
	Day       float64
	Min       float64
	Max       float64
	OWDailyID uint
}

// OWDFeelsLike is yet more of the maze
type OWDFeelsLike struct {
	gorm.Model
	Day       float64
	Night     float64
	Eve       float64
	Morn      float64
	OWDailyID uint
}

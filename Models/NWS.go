package models

import (
	"fmt"
	"log"
	"reflect"
	"time"

	helpers "Gitlab.com/adrnlnjnky/Weather/Helpers"
	pigeon "Gitlab.com/adrnlnjnky/Weather/Pigeon"
	"gorm.io/gorm"
)

//NatWeather is the top of the NW call
type NatWeather struct {
}

// NationalWeather holds API call information
type NationalWeather struct {
	ID                  string `json:"id"`
	county              string
	ForecastOffice      string
	Forecast            string `json:"forecast"`
	ForecastZone        string
	TimeZone            string
	GridX               string
	GridY               string
	FireWeatherZone     string
	VelativeLocation    string
	RadarStation        string
	ForecastHourly      string `json:"forecastHourly"`
	ObservationStations string
}

// NWSHourly owner of the hourly data
type NWSHourly struct {
	gorm.Model
	Properties []NWSHour `json:"properties"`
	// Geometry   string    `json:"geometry"`
	Geometry map[string]interface{}
}

// NWSHour just the first in a line of nested rediculousness
type NWSHour struct {
	gorm.Model
	GeneratedAt       string
	UpdateTime        string
	ValidTimes        string
	updated           string
	Elevation         map[string]interface{}
	forecastGenerator string
	periods           []NWSForecast
	NWSHourlyID       uint
	// Elevation         map[string]interface{}
}

// NWSForecast just the first in a line of nested rediculousness
type NWSForecast struct {
	gorm.Model
	endTime         time.Time
	icon            string
	isDaytime       bool
	name            string
	number          string
	shortForecast   string
	startTime       time.Time
	temperature     float64
	temeratureTrend string
	temperatureUnit string
	windDirection   string
	windSpeed       string
	NWSHour         uint
}

// NationalWeatherOptions interface to get things working.
type NationalWeatherOptions interface {
	NWSoptions()
}

// NWSoptions this gets the API address needed for a given location.  In otherwards this is the options menu so you can
// get the proper api for a given location.
// func (n *NationalWeather) NWSoptions(lat, lon float64) {
func NWSoptions(lat, lon float64) {
	jsonBlob := pigeon.NWSAPIs(lat, lon)
	info, err := helpers.ExtractJSON(jsonBlob)
	if err != nil {
		log.Println("Something went wrong getting the JSON extracted")
	}
	for k, v := range info {
		vv := reflect.TypeOf(v)
		switch {
		case vv == reflect.TypeOf("string"):
			fmt.Println(k, "\t", v)
			fmt.Println("__________")
		case k == "@context":
			fmt.Println(k, "\t", vv)
		default:
			fmt.Printf("%v\n", k)
			n := v.(map[string]interface{})
			for key, value := range n {
				if reflect.TypeOf(value) == reflect.TypeOf("string") {
					fmt.Printf("\t%v: 	| :%v\n", key, value)
				} else {
					fmt.Printf("\t%v: 	|  %v\n", key, reflect.TypeOf(value))
					// fmt.Printf("\t%v: 	| :%v\n", key, value)
				}
			}
		}
	}
}

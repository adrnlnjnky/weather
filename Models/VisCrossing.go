package models

import (
	"time"

	"gorm.io/gorm"
)

// VisualCrossing is the top level of the json call to visual crossing api
type VisualCrossing struct {
	gorm.Model
	Columns       map[string]interface{}
	RemainingCost int
	QueryCost     int
	EncuredCost   int
	Messages      map[string]interface{}
	Locations     interface{}
	// Locations     Locations `json:"-118.1"`
}

// Locations is a step in the Vis Cross json maze
type Locations struct {
	gorm.Model
	Time                 float64
	Alerts               string
	Values               []interface{}
	LocID                string `json:"id"`
	Address              string
	Name                 string
	Index                float64
	Latitude             float64
	Longitude            float64
	Distance             float64
	StationContributions string
	CurrentConditions    map[string]interface{}
	VisCrossID           uint
}

// VisualCrossingCurrent struct for current weather data from Visual Crossing
type VisualCrossingCurrent struct {
	gorm.Model
	windchill        string
	Wdir             int
	Icon             string
	Heatindex        int
	Precip           float64
	Snowdepth        float64
	Sunset           time.Time
	Temp             float64
	Datetime         time.Time
	Moonphase        float64
	Lealevelpressure float64
	Dew              float64
	Wgust            float64
	Sunrise          time.Time
	Visibility       float64
	Wspd             int
	Cloudcover       float64
	Humidity         float64
}

// VisualCrossingForecast creates the type to hold visual crossing weather data
type VisualCrossingForecast struct {
	gorm.Model
	Cloudcover       float64
	Conditions       string
	Datetime         float64
	Datetimestr      time.Time `gorm:"DatetimeStr"`
	Dew              float64
	Heatindex        string
	Humidity         float64
	Maxt             float64
	Mint             float64
	Pop              float64
	Precip           float64
	Sealevelpressure float64
	Snow             float64
	Snowdepth        float64
	Solarradiation   float64
	Sunshine         float64
	Solarenergy      float64
	Temp             float64
	Visibility       float64
	Wdir             float64
	Wgust            float64
	Windchill        float64
	Wspd             float64
	LocationsID      uint
	// VisCrossID       uint
}

// type Columns struct {
// 	VisCrossID uint
// }

// type RCost struct {
// 	VisCrossID uint
// }

// type QueryCost struct {
// 	VisCrossID uint
// }

// type Messages struct {
// 	VisCrossID uint
// }

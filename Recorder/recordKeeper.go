package recorder

import (
	"fmt"
	"log"
	"time"

	"gorm.io/gorm"
)

// RecordKeeper is going to call the recording functions.
func RecordKeeper(db *gorm.DB) {
	lat := 35.8
	lon := -108.1
	// go dailyRecorder(db, 35.8, -118.1)
	go Recorder(db, lat, lon)

}

// currentRecorder records every five minutes
func currentRecorder(db *gorm.DB, lat, lon float64) {
	i := 1
	for {
		go func() {
			fmt.Printf("CURRENT RECORDING CYCLE #: %v\n", i)
			i++
			// recordOWCurrnet(db, lat, lon)
		}()
		time.Sleep(5 * time.Minute)
		for {
		}
	}
}

// Recorder records every 24 hours
func Recorder(db *gorm.DB, lat, lon float64) {
	time.Sleep(2 * time.Second)
	for {
		i := 1
		t := time.Now()
		log.Printf("\tFORECAST RECORDING CYCLE #: %v\n ", i)
		go func() {
			fmt.Println("\t\tLaunching National Weather Hourly Go Routine")
			nWSForecastRecorder(db, "forecastHourly", lat, lon)
		}()

		go func() {
			fmt.Println("\t\tLaunching National Weather Daily Recording Go Routine")
			nWSForecastRecorder(db, "forecast", lat, lon)
		}()

		go func() {
			fmt.Println("\t\tLaunching Visual Crossing Weather Recording Go Routine")
			VisualCrossingsRecorder(db)
		}()

		go func() {
			fmt.Println("\t\tLaunching Open Weather Recording Go Routine")
			recordOpenWeather(db, lat, lon, t)
		}()

		time.Sleep(1 * time.Minute)
		i++

	}
}

package recorder

import (
	"encoding/json"
	"fmt"
	"log"
	"reflect"

	models "Gitlab.com/adrnlnjnky/Weather/Models"
	pigeon "Gitlab.com/adrnlnjnky/Weather/Pigeon"
	"gorm.io/gorm"
)

// aPIgrabberNWS this return the desired API from the Nation Weather Service for Forecasting Data
func aPIgrabberNWS(call string, lat, lon float64) (string, bool) {
	jsonBlob := pigeon.NWSAPIs(lat, lon)
	data := extractJSON(jsonBlob)
	for k, v := range data {
		if k == "properties" {
			n := v.(map[string]interface{})
			for kk, vv := range n {
				if kk == call {
					api := fmt.Sprintf("%v", vv)
					return api, true
				}
			}
		}
	}
	log.Println("Why did you hit me? You should have sent the api web address")
	return "", false
}

func nWSForecastRecorder(db *gorm.DB, opt string, lat, lon float64) {
	api, ok := aPIgrabberNWS(opt, lat, lon)
	if !ok {
		log.Println("Problem getting the National Weather Service API")
	}
	jsonBlob := pigeon.NWSForecast(api)
	weather := nWSForecastExtractor(jsonBlob)
	fmt.Println(weather)
	// recordForecastNWS(db, weather)
}

func nWSForecastExtractor(jsonBlob []byte) []models.NWSHourly {
	var w map[string]interface{}
	json.Unmarshal(jsonBlob, &w)
	for k, v := range w {
		if k == "detail" {
			note := fmt.Sprintf("Problem with the NWS resource call:\n\t%v, %v", k, v)
			log.Printf(note)
			return nil
		}
		if k == "properties" {
			fmt.Println(v)
			for x, y := range v.(map[string]interface{}) {
				if x == "periods" {
					// fmt.Println(x, y)
					for g, d := range y.(map[string]interface{}) {
						fmt.Println("Map String Test:", g, reflect.TypeOf(d))
					}
				}
			}
		}
	}

	// var test models.NWSHourly
	// json.Unmarshal(jsonBlob, &test)
	// fmt.Println("TESTING:", test.Properties)

	var weather []models.NWSHourly
	json.Unmarshal(jsonBlob, &weather)
	for a, b := range weather {
		fmt.Println("Slice Test:", a, b)
	}

	return weather
}

func recordForecastNWS(db *gorm.DB, w []models.NWSHourly) {
	db.AutoMigrate(
		&models.NWSForecast{},
		&models.NWSHour{},
		&models.NWSHourly{},
	)
	db.Debug().Model(&models.NWSHourly{}).Create(&w)
}

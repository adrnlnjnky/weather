package recorder

import (
	"fmt"
	"testing"
)

const (
	high = 290
	low  = 200
)

func TestOwExtractor(t *testing.T) {
	weather := owExtractor(lat, lon)

	t.Run("Test Latitude is correct", func(t *testing.T) {
		got := weather.Lat
		if got != lat {
			t.Errorf("%+v: Seems odd Weather Hourly Forecast is nil", got)
		}
	})

	t.Run("Test longitude is correct", func(t *testing.T) {
		got := weather.Lon
		fmt.Println(got)
		if got != lon {
			t.Errorf("%+v: Seems odd Weather Hourly Forecast is nil", got)
		}
	})

	t.Run("Test Temp is within earth tolerances and not 0", func(t *testing.T) {
		got := weather.Current.Temp
		if got == 0 {
			t.Errorf("There is a 0 reading")
		}
		if got > high {
			t.Errorf("%+v: Seems to hot to be accurate.", got)
		}
		if got < low {
			t.Errorf("%+v: Seems to cold to be accurate.", got)
		}
	})

	t.Run("Test that Feels Like is within earth tolerances", func(t *testing.T) {
		got := weather.Current.FeelsLike
		if got > high {
			t.Errorf("%+v: Seems to hot to be accurate.", got)
		}
		if got < low {
			t.Errorf("%+v: Seems to cold to be accurate.", got)
		}
	})

	t.Run("Test that Pressure is within earth tolerances", func(t *testing.T) {
		got := weather.Current.Pressure
		if got > 2000 {
			t.Errorf("%+v: Seems to hot to be accurate.", got)
		}
		if got < 500 {
			t.Errorf("%+v: Seems to cold to be accurate.", got)
		}
	})

	t.Run("Test that Humidity is within earth tolerances", func(t *testing.T) {
		got := weather.Current.Humidity
		if got > 100 {
			t.Errorf("%+v: Seems to hot to be accurate.", got)
		}
		if got < 2 {
			t.Errorf("%+v: Seems to cold to be accurate.", got)
		}
	})

	t.Run("Test that Sunrise is within earth tolerances", func(t *testing.T) {
		got := weather.Current.Sunrise
		if got == 0 {
			t.Errorf("%+v: Seems odd Sunrise is zero", got)
		}
	})

	t.Run("Test that Sunset is within earth tolerances", func(t *testing.T) {
		got := weather.Current.Sunset
		if got == 0 {
			t.Errorf("%+v: Seems odd Sunset is zero", got)
		}
	})

	t.Run("Test that Clouds is within earth tolerances", func(t *testing.T) {
		got := weather.Current.Clouds
		if got == 0 {
			t.Errorf("%+v: Seems odd Clouds is zero", got)
		}
	})

	t.Run("Test that Dt is within earth tolerances", func(t *testing.T) {
		got := weather.Current.Dt
		if got == 0 {
			t.Errorf("%+v: Seems odd Current Time is zero", got)
		}
	})

	t.Run("Test that DewPoint is within earth tolerances", func(t *testing.T) {
		got := weather.Current.DewPoint
		if got == 0 {
			t.Errorf("%+v: Seems odd dewpoint is zero", got)
		}
	})

	t.Run("Test that Wind Degree is within earth tolerances", func(t *testing.T) {
		got := weather.Current.WindDeg
		if got == 0 {
			t.Errorf("%+v: Seems odd wind degree is zero", got)
		}
	})

	t.Run("Test that Wind Gust is within earth tolerances", func(t *testing.T) {
		got := weather.Current.WindGust
		if got < 0 {
			t.Errorf("Got: %+v: Seems odd wind gust is less than zero", got)
		}
	})

	t.Run("Test that Wind Speed is within earth tolerances", func(t *testing.T) {
		got := weather.Current.WindSpeed
		if got == 0 {
			t.Errorf("%+v: Seems odd wind speed is zero", got)
		}
	})

	t.Run("Test that UVI is within earth tolerances", func(t *testing.T) {
		got := weather.Current.Uvi
		if got < 0 {
			t.Errorf("%+v: Seems odd uvi is less than 0 zero", got)
		}
	})

	t.Run("Test that Visibility is within earth tolerances", func(t *testing.T) {
		got := weather.Current.Visibility
		if got == 0 {
			t.Errorf("%+v: Seems odd Visibility is zero", got)
		}
	})

	t.Run("Test that Description is within earth tolerances", func(t *testing.T) {
		got := weather.Current.Weather.Description
		if got == "" {
			t.Errorf("%+v: Seems odd description is empty", got)
		}
	})

	t.Run("Test that Icon is within earth tolerances", func(t *testing.T) {
		got := weather.Current.Weather.Icon
		if got == "" {
			t.Errorf("%+v: Seems odd Icon is empty", got)
		}
	})

	t.Run("Test that Main is within earth tolerances", func(t *testing.T) {
		got := weather.Current.Weather.Main
		if got == "" {
			t.Errorf("%+v: Seems odd Main is empty", got)
		}
	})

	t.Run("Test that ID is within earth tolerances", func(t *testing.T) {
		got := weather.Current.Weather.Id
		if got == 0 {
			t.Errorf("%+v: Seems odd Weather ID is zero", got)
		}
	})

	t.Run("Test Daily is not nil", func(t *testing.T) {
		got := weather.Daily
		if got == nil {
			t.Errorf("%+v: Seems odd Weather Daily Forecast is nil", got)
		}
	})

	t.Run("Test Daily is not nil", func(t *testing.T) {
		got := weather.Minutely
		if got == nil {
			t.Errorf("%+v: Seems odd Weather Minutely Forecast is nil", got)
		}
	})

	t.Run("Test Daily is not nil", func(t *testing.T) {
		got := weather.Hourly
		if got == nil {
			t.Errorf("%+v: Seems odd Weather Hourly Forecast is nil", got)
		}
	})
}

// func TestRecordOWForecast(t *testing.T) {
// t.Run("Minutely is populating", func(t *testing.T) {
// 	recordOWForecast(db, lat, lon)
// 	// Yeah testing the saving is tricky and since I'm not going to test the GORM logic and I'm using gorm I guess
// 	// this test is superfluous
// })
// }

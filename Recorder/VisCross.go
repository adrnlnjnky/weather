package recorder

import (
	"encoding/json"
	"fmt"
	"log"

	models "Gitlab.com/adrnlnjnky/Weather/Models"
	pigeon "Gitlab.com/adrnlnjnky/Weather/Pigeon"
	"gorm.io/gorm"
)

// VisualCrossingsRecorder starts this silly process
func VisualCrossingsRecorder(db *gorm.DB) {
	JSONblob := pigeon.VisualCrossingWeatherAPI()
	var raw models.VisualCrossing
	json.Unmarshal(JSONblob, &raw)
	sniffer("ok")
}

// The gap is the part of JSON I can't figure out how to unmarshal into a type tree.
func dealwithTheGap(db *gorm.DB, data models.VisualCrossing, tracker []string) {
	stuff := data.Locations
	for _, v := range stuff.(map[string]interface{}) {
		for key, value := range v.(map[string]interface{}) {
			switch {
			case key == "currentConditions":
				go recordCurrent(db, value)
				continue
			case key == "values":
				go recordForecast(db, value)
				continue
			}
		}
	}
}

func extractForecast(data interface{}) []models.VisualCrossingForecast {
	stuff, _ := json.Marshal(data)
	var VCWeather []models.VisualCrossingForecast
	json.Unmarshal(stuff, &VCWeather)
	return VCWeather
}

func recordForecast(db *gorm.DB, data interface{}) {
	weather := extractForecast(data)
	db.AutoMigrate(&models.VisualCrossingForecast{})
	err := db.Model(&models.VisualCrossingForecast{}).Create(&weather)
	if err.Error != nil {
		log.Printf("Error recording Visual Crossing Forecast: %v\n", err.Error)
	}
	if err.RowsAffected < 1 {
		log.Println("no Rows Affects while recording Visual Crossing Forecast")
	}
	log.Println("Completed Visual Forecast Forecast Recording, Data should be in database and available globaly in memmory at VSWeather")

}

func extractCurrent(data interface{}) models.VisualCrossingCurrent {
	//this feels so dirty but it works so good.
	var weather models.VisualCrossingCurrent
	stuff, _ := json.Marshal(data)
	json.Unmarshal(stuff, &weather)
	return weather
}

func recordCurrent(db *gorm.DB, data interface{}) string {
	weather := extractCurrent(data)
	db.AutoMigrate(&models.VisualCrossingCurrent{})
	err := db.Model(&models.VisualCrossingCurrent{}).Create(&weather)
	if err.Error != nil {
		note := fmt.Sprintf("Error recording Visual Forecast Current: %v", err.Error)
		log.Println(note)
		return note
	}
	if err.RowsAffected < 1 {
		log.Println("Probably should have affected rows yet No Rows were affected.")
		return "No Rows affected while recording Visual Crossing Forecasts"
	}
	log.Println("Completed Visual Crossing Current Weather Recording, Data should be in database and available globaly in memmory at VSWeather")
	return "ok"
}

func sniffer(result string) []string {
	var report []string
	report = append(report, result)
	return report
}

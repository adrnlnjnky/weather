package recorder

import (
	"encoding/json"
	"fmt"
	"time"

	models "Gitlab.com/adrnlnjnky/Weather/Models"
	pigeon "Gitlab.com/adrnlnjnky/Weather/Pigeon"

	"gorm.io/gorm"
)

var (
	//Alerts is an indicator for periodic alerts.
	Alerts bool
)

func owExtractor(lat, lon float64) models.OpenWeather {
	jsonBlob := pigeon.OpenAPIForecast(lat, lon)
	var weather models.OpenWeather
	err := json.Unmarshal([]byte(jsonBlob), &weather)
	if err != nil {
		fmt.Println("owExtractor: JSON UNMARSHALING ERROR:", err)
	}
	return weather
}

// I think I'm just going to record everything every time since I'm getting the data anyway.
func recordOWCurrnet(db *gorm.DB, weather models.OpenWeather) {
	db.Model(&models.OpenWeather{}).
		Session(&gorm.Session{FullSaveAssociations: true}).
		Omit("Minutely", "Hourly", "Daily").
		Create(&weather)
}

func recordOpenWeather(db *gorm.DB, lat, lon float64, t time.Time) {
	time.Sleep(100 * time.Millisecond)
	fmt.Println("\t\t\tStarting Open Weather Forecast", t)
	weather := owExtractor(lat, lon)
	db.AutoMigrate(
		&models.OpenWeather{},
		&models.OWCurrent{},
		&models.OWCWeather{},
		&models.OWMinute{},
		&models.OWHourly{},
		&models.OWHWeather{},
		&models.OWDaily{},
		&models.OWDFeelsLike{},
		&models.OWDTemp{},
		&models.OWDWeather{},
	)

	go func() {
		fmt.Println("Open Weather Precipitation Forecast Recorded: ")
		for _, v := range weather.Minutely {
			v.OpenWeatherID = weather.ID
			db.Model(&models.OpenWeather{}).
				Session(&gorm.Session{FullSaveAssociations: true}).
				Create(&v)
			fmt.Print("P")
		}
		fmt.Print("\t Completed Precip\t")
	}()

	go func() {
		fmt.Println("Open Weather Hourly Recoring:")
		for _, v := range weather.Hourly {
			v.OpenWeatherID = weather.ID
			db.
				Model(&models.OpenWeather{}).
				Session(&gorm.Session{FullSaveAssociations: true}).
				Create(&v)
			fmt.Print("H")
		}
		fmt.Print("\tCompleted Hourly\t")
	}()

	go func() {
		fmt.Println("Open Weather Daily Forecast Recording:")
		for _, v := range weather.Daily {
			v.OpenWeatherID = weather.ID
			db.
				Model(&models.OpenWeather{}).
				Session(&gorm.Session{FullSaveAssociations: true}).
				Create(&v)
			fmt.Print("D")
		}
		fmt.Print("\tCompleted Daily\t")
	}()

	// recordCurrent(db, weather)
	db.Model(&models.OpenWeather{}).
		Session(&gorm.Session{FullSaveAssociations: true}).
		Omit("Minutely", "Hourly", "Daily").
		Create(&weather)

}

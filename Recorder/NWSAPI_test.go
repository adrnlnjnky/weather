package recorder

import (
	"testing"

	dbaccess "Gitlab.com/adrnlnjnky/Weather/Dbaccess"
)

const (
	testdb = "devdatabase"
	lat    = 35.8
	lon    = -108.1
	gridID = "ABQ"
)

var db = dbaccess.GormOpenDB(testdb)

func TestAPIgrabberNWS(t *testing.T) {
	t.Run("____", func(t *testing.T) {
		got, ok := aPIgrabberNWS("gridId", 35.8, -108.1)
		if !ok {
			t.Errorf("Problem with NWS api retrieval")
		}
		want := gridID
		if got != want {
			t.Errorf("Got Back: %v || Wanted: %v", got, want)
		}
	})
}

func TestNWSForecastExtractor(t *testing.T) {
}

func TestNWSForecastRecorder(t *testing.T) {
	nWSForecastRecorder(db, "forecastHourly", 35.8, -108.1)
}

// func TestNWSForecastRecorder(t *testing.T) {
// 	db.Raw("DROP TABLE detailed_forecast, nwshourlies, nwshours;")
// 	NWSForecastRecorder(db, "forecastHourly", lat, lon)
// t.Run("Test that Data Unmarshals", func(t *testing.T) {
// })
// }

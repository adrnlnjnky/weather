package recorder

import (
	"encoding/json"
	"fmt"
	"strings"
)

// RecordingControler is the interface for the various weather information we are recording.
type RecordingControler interface {
}

func extractJSON(jsonBlob []byte) map[string]interface{} {
	var data map[string]interface{}
	err := json.Unmarshal(jsonBlob, &data)
	if err != nil {
		fmt.Println("error:", err)
	}
	return data
}

//ExtractMap extracts the map data after it has been unmarshaled into a string of interfaces.
func ExtractMap(key string, data map[string]interface{}) map[string]interface{} {
	var pulled map[string]interface{}
	for k, v := range data {
		if k == key {
			pulled = v.(map[string]interface{})
		}
	}
	return pulled
}

// ExtractMapInterface returns an interface instead
func ExtractMapInterface(key string, data map[string]interface{}) interface{} {
	var pulled interface{}
	for k, v := range data {
		if k == key {
			pulled = v
		}
	}
	return pulled
}

// func isKey(k string) (ok bool, isArray bool) {
// 	isArray, ok = weatherKeys[k]
// 	return ok, isArray
// }

func isSunVal(k string) bool {
	return strings.Contains(k, "sun")
}
